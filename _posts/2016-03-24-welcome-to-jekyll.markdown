---
layout: post
title:  "Hello and Goodbye to Jekyll!"
date:   2016-03-24 15:32:14 -0300
categories: jekyll update
---


This is the beginning, this is the end. 

Let's dump the URLs and whatnot here for posterity. 

You have a slim file, spillgit.txt, with credentials and the basic gitlab presets. Probably also holding onto the 
spill directory, since it is set up as local with gitlab as the remote. 


Getting started with Git and GitLab
https://www.youtube.com/watch?v=7p0hrpNaJ14

mkdir, git init, README.md, git status, git add README.md

git commit -m 'first commit: add README'
git remote add origin https://gitlab.com/terre/spill.git
git remote -v
git push -u origin master

https://www.youtube.com/watch?v=TWqh9MtT4Bg

Create Your First GitLab Pages Site
https://www.youtube.com/watch?v=cERdbQ-GgOo

Done, but this quotes:
https://about.gitlab.com/2016/04/07/gitlab-pages-setup/

... which gets us to real blogging possibilities like Jekyll, and ...

**Grav**

Grav CMS Open Blog Skeleton - Setting up an Open Blog using GitLab in under 2 minutes
https://www.youtube.com/watch?v=UpzdBfQLHoQ

http://www.hibbittsdesign.org/blog/posts/2016-04-04-using-github-desktop-and-gitlab-with-grav

https://github.com/getgrav/grav-skeleton-blog-site

https://getgrav.org/blog/git-sync-plugin

https://about.gitlab.com/2017/10/12/collaborative-course-environment-gitlab-grav/

**Jekyll Proper**

https://jennifermack.net/2017/02/04/bootstrapping-a-jekyll-blog-using-gitlab/

https://about.gitlab.com/2016/04/07/gitlab-pages-setup/

https://github.com/jekyll/jekyll-admin







If you were gonna go with it, this would be one of the first things. 
https://github.com/jekyll/jekyll-admin
https://github.com/jekyll


**the original sample post**

You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. 
You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a 
web server and auto-regenerates your site when a file is updated.

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` 
and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
